%global __os_install_post %{nil}

Name:		pycharm-community
Version:	2021.2.2
Release:	3
Summary:	Intelligent Python IDE
Group:     	Applications/Development
License:    	GPLv2 and EPL-1.0 and MIT and Apache-1.1 and LGPLv2+ and Apache-2.0 and EPL-2.0
URL:		http://www.jetbrains.com/pycharm/
Source0:        pycharm-community-jbr.tar.gz
Source1:        pycharm-community-lib.tar.gz
Source2:        pycharm-community-other.tar.gz
Source3:        pycharm-community-plugin.tar.gz
Source4:    	pycharm-community.desktop
BuildRequires: 	desktop-file-utils 
Requires: 	java
Autoreq:	no

%description
The intelligent Python IDE with unique code assistance and analysis,
for productive Python development on all levels

%prep
%setup -T -q -b 0
%setup -T -D -q -b 1
%setup -T -D -q -b 2
%setup -T -D -q -b 3

%install
export QA_RPATHS=$(( 0x0004|0x0008 ))
mkdir -p %{buildroot}/opt/
cp -r %{_builddir}/pycharm-community-%{version} %{buildroot}/opt/

mkdir -p %{buildroot}/usr/share/applications/
cp -r %{SOURCE4} %{buildroot}/usr/share/applications/


%files
%defattr(-,root,root)
#%%{_bindir}/pycharm
/opt/*
%{_datadir}/applications/pycharm-community.desktop

%changelog
* Fri May 27 2022 tanyulong<tanyulong@kylinos.cn> - 2021.2.2-3
- Improve the project according to the requirements of compliance improvement

* Wed Feb 24 2021 douyan<douyan@kylinos.cn> -2021.2.2-2
- fix compile issue

* Thu Sep 23 2021 douyan<douyan@kylinos.cn> -2021.2.2-1
- update to version 2021.2.2

* Fri Sep 10 2021 douyan<douyan@kylinos.cn> -2016.2.3-2
- add desktop file and icon

* Sat Aug 14 2021 tanyulong<tanyulong@kylinos.cn> -2016.2.3
- initial pycharm-community-2016.2.3
